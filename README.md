![https://img.shields.io/gitlab/pipeline/libre-a-toi/tutoriels/master](https://img.shields.io/gitlab/pipeline/libre-a-toi/tutoriels/master)

## Source

Ce contenu fork le [https://github.com/b2a3e8/jekyll-theme-console/tree/master](jekyll-theme-console) et rajoute deux/trois features.

## Installation

Git clone, tout ça...
```console
git clone git@gitlab.com:libre-a-toi/tutoriels.git
```

## Développement

```console
cd tutoriels-master
bundle exec jekyll serve
```
Puis go sur (http://localhost:4000).

Exemple de configuration

```yaml
header_pages:
  - index.md
  - about.md

site_url: /tutoriels  # correspond au repository en fait : https://gitlab.com/libre-a-toi/tutoriels
                      # ne pas oublier le slash devant mais pas derrière

style: dark # dark (défaut) ou light

footer: 'follow us on <a href="https://twitter.com/xxx">twitter</a>'

google_analytics: UA-NNNNNNNN-N  

# ou bien :
matomo_url: https://stats.libre-a-toi.org
matomo_id: 4


```
## Contenu

Les dossiers importants : `_include`, `_layouts` et `_posts`. Les tutos sont dans `_posts` et les pages à la racine.

Les fichiers des posts doivent s'écrire ainsi `AAAA-MM-JJ-titre-sans-espace`.

Pour indiquer les métadonnées des posts, démarrer le fichier comme ceci :

```markdown
---
layout: post
title:  "Titre du post"
date:   2018-11-24 12:32:45 +0100
author: Olicat
categories: multimedia console
---
```

## License

Ce contenu est dispo en licence [MIT License](https://opensource.org/licenses/MIT).
