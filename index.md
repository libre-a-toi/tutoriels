---
title: --accueil
layout: home
permalink: /
---

# Tutoriels Libr@toi

Voici un site dédié aux stagiaires et autres débutants de la radio qui souhaitent passer de l'autre côté de la f... de la vitre. Une des premières étapes va consister à s'habituer au design de la console, dont le présent site est propédeutique (ouais, ça commence...).

Ça s'appelle "tuto" mais en vrai, il sert surtout de pense-bête pour les habitués mais si on se motive, on fera des tutos à l'occasion.

En attendant, vous avez appris le mot propédeutique. Maintenant, vous pouvez chercher les easter eggs ou retourner faire le montage des quatorze derniers épisodes de la relève.

Bisous.
