---
title: --liens
layout: page
permalink: /liens.html
---

Autres documentations pour compléter le présent tuto.

## En général
* [Le wiki de l'April sur la régie de Cause Commune](https://wiki.april.org/w/R%C3%A9gie_Cause_Commune)

## Plutôt Linux
* [Doc Ubuntu](https://doc.ubuntu-fr.org/audio)
* [Linux MAO](http://linuxmao.org/Accueil)

## Plutôt Mac
* [La chaîne Youtube Adobe France](https://www.youtube.com/user/AdobeFrance) avec par exemple [un masterclass sur Audition](https://www.youtube.com/watch?v=s8A05RoDW6Q)
