---
layout: post
title:  "Youtube DL"
date:   2018-11-24 12:32:45 +0100
author: Olicat
categories: multimedia console
---
#Téléchargement de musique sur Youtube et conversion WAV

```console
youtube-dl -x [l'url de partage de la vidéo]
```

```console
ffmpeg -i [le fichier téléchargé]  -f wav -ab 192000 -vn "nom_du_ficher.wav"
```
[MIX AUDITION]

1. Traiter le fichier brut en monopiste (coupes)
2. Traiter les insertions en multipistes (musiques, jingles...)
3. sélectionner toutes les pistes. menu CLIP / Match clip loudness
4. Menu multitrack / mixdown session to new track
5. Traiter le mix monopiste : effects / Filtres and EQ / Parametric Equalizer / Voice enhancer

Ce dernier traitement remet les musiques au niveau des voix

that's all
