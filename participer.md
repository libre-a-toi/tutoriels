---
title: --participer
layout: page
permalink: /participer.html
---

# Signaler un bug ou une erreur dans un tutoriel
[Faire un ticket](https://gitlab.com/libre-a-toi/tutoriels/issues/new)
 * pour signaler un bug choisir le template bug.
 * pour suggérer la rédaction d'un tuto, choisir le template request.  

## Rédiger un tutoriel : méthode bourrine et directe
Cloner le dépôt directement et faire un merge request.
```
git clone git@gitlab.com:libre-a-toi/tutoriels.git
```
Aller dans le dossier posts.

## Rédiger un tutoriel : Méthode simple
Créer un fichier .md à soumettre aux collègues. Utiliser un éditeur tel que QownNote (à connecter avec https://drive.cause-commune.fm) ou Marp avant de partager le fichier. Consulter [le guide de syntaxe markdown](https://www.christopheducamp.com/2014/09/18/love-markdown/).

## dans tous les cas
Nommer le fichier de la manière suivante : AAAA-MM-JJ-titre-sans-espace.
Préciser les métadonnées du nouveau tuto en début de fichier comme ceci :
```
---
layout: post
title:  "Exemple de super titre"
date:   2018-11-24 12:32:45 +0100
author: Olicat
categories: console mac audition
---
Bla bla bla mon super tuto....
```
